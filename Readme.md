##### Current Version: 0.1-beta

## Synopsis
Working on a membership site, we have to create multiple level of users. Usually we do not want the users to have access to the WP-Admin panel because it is not customized for their experience. Rather we have put everything necessary (such as edit profile page), user dashboard etc, on the front-end. For that reason we have to disable the WP Admin Bar for all except the administrators ( hidewpbar wordpress plugin).

The following plugin disables the wordpress admin bar for all users upon activation.

## Requirements
PHP v5 and above

## Installation
- Download the \Executable\hidewpbar.zip
- Upload the hidewpbar.zip to your wordpress site through the plugin page.

## Usage
Activate the plugin to hide the wordpress admin bar for all users except the administrators.
Disable the plugin and everything will be restored to default.

## Example
[ Empty ]

## License
Please check the LICENSE.md file